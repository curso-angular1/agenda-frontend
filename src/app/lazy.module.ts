import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { AgregarComponent } from './components/agregar/agregar.component';
import { TablaComponent } from './components/tabla/tabla.component';

import { lazyRoutingModule } from './lazy.routing';
import { AppMaterialModule } from './app-material.module';

import { FechaPipe } from './pipes/fecha.pipe';


@NgModule({
  imports: [
    CommonModule,
    lazyRoutingModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FormsModule
    
  ],
  declarations: [AgregarComponent,TablaComponent,FechaPipe]
})
export class LazyModule { }
