import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Contacto } from '../../model/Contacto.model';
import { AgendaService } from 'src/app/services/agenda.service';



@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.scss'],
  providers: [AgendaService]
})
export class TablaComponent implements OnInit {
  Contactos: Contacto[] = [];
  dataSource: MatTableDataSource<Contacto>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  displayedColumns: string[] = ['nombre', 'telefono', 'email', 'fecha_nac'];


  constructor(private _agendaService: AgendaService) {

  }



  ngOnInit() {
    this.get_contactos();
  }
  get_contactos() {
    this._agendaService.get_contactos().subscribe(resp => {
      this.Contactos = resp.contactos;
      this.dataSource = new MatTableDataSource<Contacto>(this.Contactos);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
      }, 0);
    });
  }

}
