import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AgendaService } from 'src/app/services/agenda.service';
import { SweetalertService } from 'src/app/services/sweetalert.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss']
})
export class AgregarComponent implements OnInit {

  public contactoform: FormGroup;

  constructor(
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private _agendaService: AgendaService,
    private _sweetalert:SweetalertService
  ) { }

  ngOnInit(): void {
    this.contactoform = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      telefono: ['', [Validators.required]],
      fecha_nac: ['', [Validators.required]],
    });
  }
  get nombre() { return this.contactoform.get('nombre') }
  get email() { return this.contactoform.get('email') }
  get telefono() { return this.contactoform.get('telefono') }
  get fecha_nac() { return this.contactoform.get('fecha_nac') }



  crear() {
    if(this.contactoform.valid){
      var current_datetime = new Date(this.contactoform.get('fecha_nac').value);
      let formatted_date = current_datetime.getFullYear() + "-" +this.appendLeadingZeroes((current_datetime.getMonth() + 1)) + "-" + this.appendLeadingZeroes(current_datetime.getDate())

      this.contactoform.get('fecha_nac').setValue(formatted_date); //Poner un nuevo valor al campo de nuestro formulario
      this._agendaService.registrarcontacto(this.contactoform.value)
      .subscribe((res: any) => {
        this._sweetalert.alerta();
/*         this.openSnackBar('Se ha creado el contacto con exito', 'cerrar')
 */
      }, (error: any) => {
/*         this.openSnackBar("Ha ocurrido un error", 'cerrar')
 */      })

      this.contactoform.reset();
      
    }
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }

   appendLeadingZeroes(n){
    if(n <= 9){
      return "0" + n;
    }
    return n
  }

}
