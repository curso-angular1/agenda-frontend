import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fecha'
})
export class FechaPipe implements PipeTransform {

  transform(value: String, ...args: unknown[]): String {


    let fechas = value.split('-');
    let r = fechas[2] + " de ";
    switch (fechas[1]) {
      case "01":
        r += "Enero";
        break;
      case "02":
        r += "Febrero";
        break;
      case "03":
        r += "Marzo";
        break;
      case "04":
        r += "Abril";
        break;
      case "05":
        r += "Mayo";
        break;
      case "06":
        r += "Junio";
        break;
      case "07":
        r += "Julio";
        break;
      case "08":
        r += "Agosto";
        break;
      case "09":
        r += "Septiembre";
        break;
      case "10":
        r += "Octubre";
        break;
      case "11":
        r += "Noviembre";
        break;
      case "12":
        r += "Diciembre";
        break;
    }
    r+=" de " + fechas[0]; 

    return r;
  }

}
