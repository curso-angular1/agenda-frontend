import {Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';


export const routes:Routes=[
    { path: '', component: HomeComponent,pathMatch:'full' },
    { path: 'miapp',loadChildren: ()=> import('./lazy.module').then(m=>m.LazyModule) },
]