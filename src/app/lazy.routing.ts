import {  Routes,RouterModule  } from '@angular/router';

import { AgregarComponent } from './components/agregar/agregar.component';
import { TablaComponent } from './components/tabla/tabla.component';
import { NgModule } from '@angular/core';

const lazyroutes: Routes = [
    { path: '', component: AgregarComponent },
    { path: 'visualizar', component: TablaComponent }
];

@NgModule({
imports:[RouterModule.forChild(lazyroutes)],
exports:[RouterModule]
})

export class lazyRoutingModule{}