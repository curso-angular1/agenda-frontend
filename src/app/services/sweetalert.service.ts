import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Injectable({
  providedIn: 'root'
})
export class SweetalertService {

  constructor() { }

    alerta(){
      Swal.fire({
        title: 'Exito!',
        text: 'El contacto ha sido agregado',
        icon: 'success',
        confirmButtonText: 'Cool'
      })
    }
}
