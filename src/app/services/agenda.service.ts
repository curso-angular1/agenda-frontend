import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'
import { url_dominio } from '../services/global';
import { Contacto } from '../model/Contacto.model';
@Injectable({
  providedIn: 'root'
})
export class AgendaService {
  public url: string

  constructor(private _http: HttpClient) {
    this.url = url_dominio
  }

  public get_contactos(): Observable<any> {
    return this._http.get(`${this.url}/contactos`);
  }
  
  public registrarcontacto(contacto):Observable<any> {
    let params = JSON.stringify(contacto);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.post(`${this.url}/crear-contacto`, params, { headers });
  }




}
